﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ConsoleAppWebservice
{
    public class SoapClientConfig
    {
        public string ServiceUrl { get; private set; }
        public X509Certificate2 Certificate { get; private set; }
        public SignAlgorithm SignAlgorithm { get; private set; }

        public SoapClientConfig(string url, X509Certificate2 certificate, SignAlgorithm signAlgorithm)
        {
            ServiceUrl = url;
            Certificate = certificate;
            SignAlgorithm = signAlgorithm;
        }

        public SoapClientConfig(string url, string certificateKeystorePath, string certificateKeystorePass, SignAlgorithm signAlgorithm)
        {
            ServiceUrl = url;
            Certificate = new X509Certificate2(certificateKeystorePath, certificateKeystorePass);
            SignAlgorithm = signAlgorithm;
        }

        public void SetServiceUrl(string url)
        {
            ServiceUrl = url;
        }

        public void SetCertificate(string certificateKeystorePath, string certificateKeystorePass)
        {
            Certificate = new X509Certificate2(certificateKeystorePath, certificateKeystorePass);
        }

        public void SetCertificate(X509Certificate2 certificate)
        {
            Certificate = certificate;
        }
    }
}
