﻿using SeguroViagem;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConsoleAppWebservice
{
    public static class SoapClient
    {
        /// <summary>
        /// Método de chamada ao Webservice
        /// </summary>
        /// <typeparam name="T">Objetos de Response do WSDL</typeparam>
        /// <param name="typedRequestObject">Objetos de Request do WSDL</param>
        /// <param name="methodName">Nome do método do WSLD contidos na interface SeguroViagemPort</param>
        /// <returns></returns>
        public static T BeginRequest<T>(SoapClientConfig soapClientConfig, object typedRequestObject, string methodName)
        {
            var type = typeof(SeguroViagemPort);

            string action = ((System.Reflection.TypeInfo)type).DeclaredMembers.Where(x => x.Name == methodName).SingleOrDefault()
                .CustomAttributes.Where(x => x.AttributeType.FullName == "System.ServiceModel.OperationContractAttribute").SingleOrDefault()
                .NamedArguments.Where(x => x.MemberName == "Action").SingleOrDefault().TypedValue.Value?.ToString();

            string returnedValue = CallWebService(soapClientConfig, action, typedRequestObject);
            var returnedSoapXml = new XmlDocument();
            returnedSoapXml.LoadXml(returnedValue);

            var doc = new XmlDocument();
            string responseTypeName = typeof(T).Name;
            var builder = new StringBuilder();
            foreach (XmlElement node in returnedSoapXml.GetElementsByTagName("SOAP-ENV:Body")[0].ChildNodes[0].ChildNodes)
            {
                builder.Append(node.OuterXml);
            }
            string xmlBody = builder.ToString();
            string xmlBodyStr = $"<{responseTypeName}>{xmlBody}</{responseTypeName}> ";

            var xRoot = new XmlRootAttribute
            {
                ElementName = responseTypeName,
                IsNullable = true
            };

            var serializer = new XmlSerializer(typeof(T), xRoot);
            var stringReader = new StringReader(xmlBodyStr);
            T obj = (T)serializer.Deserialize(stringReader);

            return obj;
        }

        private static string CallWebService(SoapClientConfig soapClientConfig, string action, object typedRequest)
        {
            var soapEnvelopeXml = SoapClientHelper.CreateSoapEnvelope(typedRequest);

            var signedSoapEnvelopeXml = SoapClientHelper.Sign(soapClientConfig.Certificate, soapClientConfig.SignAlgorithm, soapEnvelopeXml);

            var webRequest = SoapClientHelper.CreateWebRequest(soapClientConfig.ServiceUrl, action);

            webRequest.Headers.Add("Content-Length", signedSoapEnvelopeXml.OuterXml.Length.ToString());

            using (Stream requestStream = webRequest.GetRequestStream())
            {
                byte[] paramBytes = Encoding.UTF8.GetBytes(signedSoapEnvelopeXml.OuterXml);
                requestStream.Write(paramBytes, 0, paramBytes.Length);
            }

            WebResponse webResponse = webRequest.GetResponse();
            using StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8);
            string result = "";
            result = myStreamReader.ReadToEnd();
            return result;
        }
    }
}
