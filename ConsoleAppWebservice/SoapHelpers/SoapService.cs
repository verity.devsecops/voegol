﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ConsoleAppWebservice.SoapHelpers
{
    public class SoapService
    {
        public SoapClientConfig SoapClientConfig { get; private set; }

        public SoapService(SoapClientConfig soapClientConfig)
        {
            SoapClientConfig = soapClientConfig;
        }
    }
}
