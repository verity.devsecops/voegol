﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConsoleAppWebservice
{
    public static class SoapClientHelper
    {
        public static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.Headers.Add("Accept-Encoding", "gzip, deflate");
            webRequest.ContentType = "text/xml;charset=UTF-8";
            webRequest.Method = "POST";
            return webRequest;
        }

        public static XmlDocument CreateSoapEnvelope(object typedRequest)
        {
            var typedRequestAttributes = typedRequest.GetType().GetCustomAttributesData();
            string wrapperName = null;
            string wrapperNamespace = null;

            var customAttributeData = typedRequestAttributes.Where(x => x.AttributeType.FullName == "System.ServiceModel.MessageContractAttribute").SingleOrDefault();
            wrapperName = customAttributeData.NamedArguments.Where(x => x.MemberName == "WrapperName").SingleOrDefault().TypedValue.Value?.ToString();
            wrapperNamespace = customAttributeData.NamedArguments.Where(x => x.MemberName == "WrapperNamespace").SingleOrDefault().TypedValue.Value?.ToString();

            var xmlSerializer = new XmlSerializer(typedRequest.GetType());
            string xmlString = null;
            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xmlSerializer.Serialize(writer, typedRequest);
                    xmlString = sww.ToString();
                }
            }

            XmlDocument objectXml = new XmlDocument();
            objectXml.LoadXml(xmlString);
            StringBuilder builder = new StringBuilder();
            foreach (XmlElement node in objectXml.ChildNodes[1].ChildNodes)
            {
                builder.Append(node.OuterXml);
            }
            string xmlBody = builder.ToString();


            XmlDocument soapEnvelopeDocument = new XmlDocument();
            soapEnvelopeDocument.LoadXml(
            $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:loc=""{wrapperNamespace}"">
                    <soapenv:Header/>                    
                    <soapenv:Body>
                        <loc:{wrapperName}>
                            {xmlBody}
                        </loc:{wrapperName}>
                    </soapenv:Body>
                </soapenv:Envelope>");
            return soapEnvelopeDocument;
        }

        public static XmlDocument Sign(X509Certificate2 certificate, SignAlgorithm signAlgorithm, XmlDocument xmlDoc)
        {
            SoapSigner signer = new SoapSigner();
            XmlNamespaceManager ns = new XmlNamespaceManager(xmlDoc.NameTable);
            ns.AddNamespace("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
            XmlElement body = xmlDoc.DocumentElement.SelectSingleNode(@"//soapenv:Body", ns) as XmlElement;
            if (body == null)
                throw new ApplicationException("No body tag found");
            body.SetAttribute("Id", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "_1");

            XmlDocument tempDoc = new XmlDocument();
            tempDoc.PreserveWhitespace = true;
            tempDoc.LoadXml(xmlDoc.OuterXml);

            return signer.SignMessage(tempDoc, certificate, signAlgorithm);
        }
    }
}
