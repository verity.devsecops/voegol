﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppWebservice.Dtos
{
    public class PercentualComissaoDto
    {
        public bool IsPadrao { get; set; }
        public decimal ValorCorretagem { get; set; }
        public decimal ValorAgenciamento { get; set; }
        public decimal ValorPrestacaoServico { get; set; }
    }
}
