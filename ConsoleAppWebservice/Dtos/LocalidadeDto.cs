﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppWebservice.Dtos
{
    public class LocalidadeDto
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}
