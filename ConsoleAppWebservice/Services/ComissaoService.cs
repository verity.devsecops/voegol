﻿using ConsoleAppWebservice.Dtos;
using ConsoleAppWebservice.Mappers;
using ConsoleAppWebservice.SoapHelpers;
using SeguroViagem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ConsoleAppWebservice.Services
{
    public class ComissaoService : SoapService
    {
        public ComissaoService(SoapClientConfig soapClientConfig) : base(soapClientConfig) { }

        public List<PercentualComissaoDto> GetPercentuaisComissao()
        {
            var request = new GetPercentuaisComissaoRequest();

            var response = SoapClient.BeginRequest<GetPercentuaisComissaoResponse>(base.SoapClientConfig, request, "GetPercentuaisComissao");

            var percentuais = response.percentuais.ToList();

            var percentuaisDto = MapperProvider.GetInstance().Map<List<PercentualComissaoDto>>(percentuais);

            return percentuaisDto;
        }
    }
}
