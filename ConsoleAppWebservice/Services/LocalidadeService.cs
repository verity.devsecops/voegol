﻿using ConsoleAppWebservice.Dtos;
using ConsoleAppWebservice.Mappers;
using ConsoleAppWebservice.SoapHelpers;
using SeguroViagem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ConsoleAppWebservice.Services
{
    public class LocalidadeService : SoapService
    {
        public LocalidadeService(SoapClientConfig soapClientConfig) : base(soapClientConfig) { }

        public List<LocalidadeDto> GetLocalidades()
        {
            var request = new GetLocalidadesRequest
            {
                tipoLocalidade = LocalidadesRequestTypeTipoLocalidade.D,
                categoriaViagem = LocalidadesRequestTypeCategoriaViagem.I,
                identificadorSistemaOrigem = LocalidadesRequestTypeIdentificadorSistemaOrigem.CO
            };

            var response = SoapClient.BeginRequest<GetLocalidadesResponse>(base.SoapClientConfig, request, "GetLocalidades");

            var localidades = response.localidades.ToList();

            var localidadesDto = MapperProvider.GetInstance().Map<List<LocalidadeDto>>(localidades);

            return localidadesDto;
        }
    }
}
