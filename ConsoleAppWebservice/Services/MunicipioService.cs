﻿using ConsoleAppWebservice.Dtos;
using ConsoleAppWebservice.Mappers;
using ConsoleAppWebservice.SoapHelpers;
using SeguroViagem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ConsoleAppWebservice.Services
{
    public class MunicipioService : SoapService
    {
        public MunicipioService(SoapClientConfig soapClientConfig) : base(soapClientConfig) { }

        public List<MunicipioDto> GetMunicipios()
        {
            var request = new GetMunicipiosRequest
            {
                identificadorSistemaOrigem = LocalidadesRequestTypeIdentificadorSistemaOrigem.RE,
                estado = MunicipiosRequestTypeEstado.SE,
                codigoRepresentante = "10001242"
            };

            var response = SoapClient.BeginRequest<GetMunicipiosResponse>(base.SoapClientConfig, request, "GetMunicipios");

            var municipios = response.municipios.ToList();

            var municipiosDto = MapperProvider.GetInstance().Map<List<MunicipioDto>>(municipios);

            return municipiosDto;
        }
    }
}
