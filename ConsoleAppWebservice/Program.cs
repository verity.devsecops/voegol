﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Linq;
using SeguroViagem;
using System.Xml.Serialization;
using ConsoleAppWebservice.Services;
using Microsoft.Extensions.Configuration;

namespace ConsoleAppWebservice
{
    class Program
    {
        public static IConfigurationRoot configuration;

        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true);
            builder.AddJsonFile($"appsettings.json", optional: false);
            configuration = builder.Build();

            string certificadoKeystorePath = configuration["CertificadoKeystorePath"];
            string certificadoKeystorePass = configuration["CertificadoKeystorePass"];
            SoapClientConfig soapConfig = new SoapClientConfig(configuration["UrlWebService"], 
                certificadoKeystorePath, certificadoKeystorePass, SignAlgorithm.DSA);

            var localidadeService = new LocalidadeService(soapConfig);
            var localidades = localidadeService.GetLocalidades();
            Console.WriteLine($"Localidades encontradas: {localidades.Count}");

            var municipioService = new MunicipioService(soapConfig);
            var municipios = municipioService.GetMunicipios();
            Console.WriteLine($"Localidades encontradas: {municipios.Count}");

            var comissaoService = new ComissaoService(soapConfig);
            var percentuaisComissao = comissaoService.GetPercentuaisComissao();
            Console.WriteLine($"Localidades encontradas: {percentuaisComissao.Count}");

        }
    }
}
