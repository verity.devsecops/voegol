﻿using AutoMapper;
using ConsoleAppWebservice.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppWebservice.Mappers
{
    public class MunicipioMapperProfile : Profile
    {
        public MunicipioMapperProfile()
        {
            CreateMap<SeguroViagem.Municipio, MunicipioDto>()
                .ForMember(s => s.Id,
                    opt => opt.MapFrom(src => src.idMunicipio))
                .ForMember(s => s.Descricao,
                    opt => opt.MapFrom(src => src.descricao));
        }
    }
}