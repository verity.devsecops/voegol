﻿using AutoMapper;
using ConsoleAppWebservice.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppWebservice.Mappers
{
    public class PercentualComissaoMapperProfile : Profile
    {
        public PercentualComissaoMapperProfile()
        {
            CreateMap<SeguroViagem.Percentual, PercentualComissaoDto>()
                .ForMember(s => s.IsPadrao,
                    opt => opt.MapFrom(src => src.padrao))
                .ForMember(s => s.ValorCorretagem,
                    opt => opt.MapFrom(src => src.corretagem))
                .ForMember(s => s.ValorAgenciamento,
                    opt => opt.MapFrom(src => src.agenciamento))
                .ForMember(s => s.ValorPrestacaoServico,
                    opt => opt.MapFrom(src => src.prestacaoServico));
        }
    }
}