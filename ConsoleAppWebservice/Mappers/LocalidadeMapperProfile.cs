﻿using AutoMapper;
using ConsoleAppWebservice.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppWebservice.Mappers
{
    public class LocalidadeMapperProfile : Profile
    {
        public LocalidadeMapperProfile()
        {
            CreateMap<SeguroViagem.Localidade, LocalidadeDto>()
                .ForMember(s => s.Id,
                    opt => opt.MapFrom(src => src.idLocalidade))
                .ForMember(s => s.Descricao,
                    opt => opt.MapFrom(src => src.descricao));
        }
    }
}