﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppWebservice.Mappers
{
    public static class MapperProvider
    {
        private static IMapper _instance;

        public static IMapper GetInstance()
        {
            if (_instance == null)
            {
                LoadProfile();
            }

            return _instance;
        }

        private static void LoadProfile()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddMaps(typeof(Program).Assembly);
            });

            _instance = configuration.CreateMapper();
        }
    }
}
