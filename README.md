# Voegol

## Software utilizado
Microsoft Visual Studio 2019

## Versão do .Net Framework
.Net Core 3.1 (LTS)

## Instruções:
O projeto foi desenvolvido como Console Application.

### Debug
Procurar pelo arquivo `Program.cs` na raiz do projeto, colocar breakpoints no método Main e executar como Debug para inspecionar as variáveis de resposta ou os passos do processo.
>Utilizar o código do Main como base

```c#
static void Main(string[] args)
{
    ...
}
```

### Configurando o serviço
Será necessário configurar o serviço informando a URL e o certificado que será usado para assinar a requisição.

Para isso, será necessário instanciar um objeto do tipo `SoapClientConfig`, passando como parâmetros Url, certificado e algoritmo do certificado.

O certificado é do tipo `X509Certificate2` e existem duas maneiras de fornecê-lo:
* Passando path e senha do certificado no construtor do `SoapClientConfig`.
* Instanciando um `X509Certificate2` manualmente, seja através de certificado instalado na máquina quanto informando o path e senha, e em seguida, passar no contrutor do `SoapClientConfig`.

### Exemplo de instância do certificado
```c#
X509Certificate2 cert = new X509Certificate2(certificatePath, certificatePass);
```

### Exemplo de instância do `SoapClientConfig`
```c#
string certificadoKeystorePath = configuration["CertificadoKeystorePath"];
string certificadoKeystorePass = configuration["CertificadoKeystorePass"];
SoapClientConfig soapConfig = new SoapClientConfig(configuration["UrlWebService"], certificadoKeystorePath, certificadoKeystorePass, SignAlgorithm.DSA);
```                

>Algumas configurações foram adicionadas ao `appsettings.json` presente na raiz do projeto.

As classes de fazem a consulta devem herdar a classe `SoapService`.

Para consultar, utilizar o método `SoapClient.BeginRequest`, onde deverão conter os seguintes parâmetros:
1. `SoapClientConfig`.
2. Objetos com sufixo `Request` importados do WSDL pelo WCF Connected Services. 
3. Nome do SoapMethod presente no WSDL presente na tag `wsdl:operation`
```xml
<wsdl:operation name="GetLocalidades">
<wsdl:input message="tns:GetLocalidadesRequest" name="GetLocalidadesRequest"/>
<wsdl:output message="tns:GetLocalidadesResponse" name="GetLocalidadesResponse"/>
</wsdl:operation>
```
Outra forma de obter o nome do método é instanciar o objeto `SeguroViagemPortClient` e visualizar os métodos disponíveis e quais os objetos e `Request` e `Response` correspondentes.
```c#
SeguroViagem.SeguroViagemPortClient client = new SeguroViagemPortClient();
client.GetLocalidades()
```            
4. Passar como tipo genérico tipos com sufixo `Response` importados do WSDL pelo WCF Connected Services.

Utilizando como exemplo o método do WSDL `GetLocalidades` (que serviu como exemplo logo acima), a chamada do serviço ficará parecida com o código abaixo:


```c#
public class LocalidadeService : SoapService
{
    public LocalidadeService(SoapClientConfig soapClientConfig) : base(soapClientConfig) { }

    public List<LocalidadeDto> GetLocalidades()
    {
        var request = new GetLocalidadesRequest
        {
            tipoLocalidade = LocalidadesRequestTypeTipoLocalidade.D,
            categoriaViagem = LocalidadesRequestTypeCategoriaViagem.I,
            identificadorSistemaOrigem = LocalidadesRequestTypeIdentificadorSistemaOrigem.CO
        };

        var response = SoapClient.BeginRequest<GetLocalidadesResponse>(base.SoapClientConfig, request, "GetLocalidades");

        var localidades = response.localidades.ToList();

        var localidadesDto = MapperProvider.GetInstance().Map<List<LocalidadeDto>>(localidades);

        return localidadesDto;
    }
}
```